## __PXE via Netboot avec un Raspberry PI 4 🏆__

* netboot.xyz permet le démarrage en réseau de différents systèmes d'exploitation via le même appareil. Le menu est déjà préconfiguré avec les systèmes les plus utilisées et plus… La version conteneurisée dispose d'une interface WEB afin d'ajouter des miroirs ou modifier le menu.

## __Composantes :__

* Nginx, TFTP-HPA, Syslog, Web Interface (netboot.xyz)
---
##### __Installation Docker (Debian 12 Bookworm):__

```
sudo apt update && apt upgrade -y
```

```
sudo apt install docker.io
```

##### __Récupération de l'image:__

```
docker pull ghcr.io/netbootxyz/netbootxyz
```

##### __docker-cli (à modifier selon votre configuration):__

1. Cette commande permet de crée un conteneur Docker nommé "netbootxyz" en utilisant l'image Docker netbootxyz disponible sur ghcr.io
2. Le conteneur sera automatiquement redémarré sauf arrêt manuel.

```shell
docker run -d \
  --name=netbootxyz \
  -e MENU_VERSION=2.0.76             `# optional` \
  -e NGINX_PORT=80                   `# optional` \
  -e WEB_APP_PORT=3000               `# optional` \
  -p 3000:3000                       `# sets web configuration interface port, destination should match ${WEB_APP_PORT} variable above.` \
  -p 69:69/udp                       `# sets tftp port` \
  -p 8080:80                         `# optional, destination should match ${NGINX_PORT} variable above.` \
  -v /local/path/to/config:/config   `# optional` \
  -v /local/path/to/assets:/assets   `# optional` \
  --restart unless-stopped \
  ghcr.io/netbootxyz/netbootxyz
```

**__Mise à jour netbootxyz :__**

1. Afin d'installer la dernière version en date de netboot.xyz :

   ```
   docker-compose pull netbootxyz
   ```
---
## Configuration DHCP :

Installation d'un serveur DHCP de votre choix, ici isc-dhcp-server :

```
sudo apt install isc-dhcp-server
```

Pensez à modifier votre interface si nécessaire dans : /etc/default/isc-dhcp-server

```
INTERFACESv4="eth0"
```

  
Toute la configuration du DHCP se passe dans /etc/dhcp/dhcpd.conf

```
option arch code 93 = unsigned integer 16;

subnet 192.168.0.0 netmask 255.255.255.0 {
  range 192.168.0.34 192.168.0.254;       # Change this range as appropriate for your network
  next-server 192.168.0.33;               # Change this to the address of your DHCP server
  option subnet-mask 255.255.255.0;
  option routers 192.168.0.1;             # Change this to the address of your router
  option broadcast-address 192.168.0.255;
  option domain-name "mynetwork.lan";     # This is optional
  option domain-name-servers 1.1.1.1;
  if exists user-class and ( option user-class = "iPXE" ) {
    filename "http://boot.netboot.xyz/menu.ipxe";
  } elsif option arch = encode-int ( 16, 16 ) {
    filename "http://boot.netboot.xyz/ipxe/netboot.xyz.efi";
    option vendor-class-identifier "HTTPClient";
  } elsif option arch = 00:07 {
    filename "netboot.xyz.efi";
  } else {
    filename "netboot.xyz.kpxe";
  }
}
```

N'oubliez pas d'adapter les adresses IP, plages d'adresses et les path selon vos configurations.   
PS : Il faut spécifier un DNS publique comme (1.1.1.1/8.8.8.8) et non local comme (192.168.0.100).